use opentelemetry::{
    sdk, 
    Key, 
    trace::{TraceError, Tracer, TraceContextExt}, global
};
use opentelemetry_jaeger;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync + 'static>> {
    let tracer = init_tracer()?;
    tracer.in_span("test-operation", |cx| {
        cx.span().set_attribute(Key::new("message").string("foo bar"));
    });

    // Wait for the batch processor pipeline to shut down.
    global::shutdown_tracer_provider();

    Ok(())
}

fn init_tracer() ->  Result<sdk::trace::Tracer, TraceError>{
    opentelemetry_jaeger::new_collector_pipeline()
        .with_endpoint("http://localhost:14268/api/traces")
        .with_reqwest()
        .with_service_name("jaeger-test-service")
        .install_batch(opentelemetry::runtime::Tokio)
}